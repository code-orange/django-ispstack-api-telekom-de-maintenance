from django.urls import path
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView

from django_ispstack_api_telekom_de_maintenance.django_ispstack_api_telekom_de_maintenance.services.v5.views import (
    CarrierEntstoerungService as CarrierEntstoerungService_v5,
)

urlpatterns = [
    path(
        "v5/in/submit",
        DjangoView.as_view(
            services=[CarrierEntstoerungService_v5],
            tns="http://wholesale.telekom.de/ess/v5/service/carrier",
            in_protocol=Soap11(validator="soft"),
            out_protocol=Soap11(),
            name="CarrierEntstoerungPortType",
        ),
    ),
]
