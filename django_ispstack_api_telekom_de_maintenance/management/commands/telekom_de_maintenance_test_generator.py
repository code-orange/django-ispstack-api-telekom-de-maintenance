from django.core.management.base import BaseCommand

from django_ispstack_access.django_ispstack_access.models import IspAccessCpeMaintenance
from django_ispstack_api_telekom_de_maintenance.django_ispstack_api_telekom_de_maintenance.services.v5 import (
    api_client,
)


class Command(BaseCommand):
    help = "Test run for xml generation"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/est_adsl.xml", "wt"
        ).write(
            api_client.gen_maintenance_request(
                IspAccessCpeMaintenance.objects.get(id=20210417000001)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/est_vdsl.xml", "wt"
        ).write(
            api_client.gen_maintenance_request(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/rek_adsl.xml", "wt"
        ).write(
            api_client.gen_maintenance_complaint(
                IspAccessCpeMaintenance.objects.get(id=20210417000001)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/rek_vdsl.xml", "wt"
        ).write(
            api_client.gen_maintenance_complaint(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/storno.xml", "wt"
        ).write(
            api_client.gen_maintenance_cancellation(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/termin_antwort.xml",
            "wt",
        ).write(
            api_client.gen_maintenance_schedule_response(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/diagnose_auftrag.xml",
            "wt",
        ).write(
            api_client.gen_maintenance_diagnosis(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/support_auftrag.xml",
            "wt",
        ).write(
            api_client.gen_maintenance_support(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/gef_diag_start.xml",
            "wt",
        ).write(
            api_client.gen_maintenance_guided_diagnosis_start(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/gef_diag_fortsetzen.xml",
            "wt",
        ).write(
            api_client.gen_maintenance_guided_diagnosis_continue(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        open(
            "data/django_ispstack_api_telekom_de_maintenance/test/gef_diag_beenden.xml",
            "wt",
        ).write(
            api_client.gen_maintenance_guided_diagnosis_finish(
                IspAccessCpeMaintenance.objects.get(id=20210417000012)
            )
        )

        # open('support.json', 'wt').write(
        #     json.dumps(xmltodict.parse(open('SupportAuftrag.xml', 'rb')), indent=4)
        # )

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
