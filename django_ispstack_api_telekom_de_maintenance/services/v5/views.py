from spyne.decorator import rpc
from spyne.model.primitive import AnyDict, Unicode
from spyne.service import ServiceBase


class CarrierEntstoerungService(ServiceBase):
    @rpc(
        Unicode,
        _returns=AnyDict,
        _in_message_name="annehmenEntstoerauftragMeldungRequest",
        _out_message_name="annehmenEntstoerauftragMeldungResponse",
    )
    def annehmenEntstoerauftragMeldung(ctx, annehmenEntstoerauftragMeldungRequest):
        return {"successful": "true"}

    @rpc(
        Unicode,
        _returns=AnyDict,
        _in_message_name="annehmenDiagnoseauftragMeldungRequest",
        _out_message_name="annehmenDiagnoseauftragMeldungResponse",
    )
    def annehmenDiagnoseauftragMeldung(ctx, annehmenDiagnoseauftragMeldungRequest):
        return {"successful": "true"}

    @rpc(
        Unicode,
        _returns=AnyDict,
        _in_message_name="annehmenSupportauftragMeldungRequest",
        _out_message_name="annehmenSupportauftragMeldungResponse",
    )
    def annehmenSupportauftragMeldung(ctx, annehmenSupportauftragMeldungRequest):
        return {"successful": "true"}
