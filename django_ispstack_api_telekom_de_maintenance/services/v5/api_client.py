import json
from datetime import datetime

import requests
import xmltodict
from OpenSSL import crypto
from django.conf import settings
from zeep import Client
from zeep.transports import Transport
from zeep.wsse import BinarySignature

from django_ispstack_access.django_ispstack_access.models import IspAccessCpeMaintenance


def fetch_cert_details():
    cert_file = settings.TELEKOM_DE_MAINTENANCE_CERT_FILE_PUB
    cert = crypto.load_certificate(crypto.FILETYPE_PEM, open(cert_file, "rb").read())
    issuer = cert.get_issuer()
    issued_by = issuer.CN
    serial = cert.get_serial_number()

    return {
        "issuer": issued_by,
        "serial": serial,
    }


def gen_maintenance_request(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_request.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:annehmenEntstoerauftragRequest"]["Control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["ev:annehmenEntstoerauftragRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "ExterneStoerungsnummer"
    ] = cpe_maintenance.id

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"]["Kunde"][
        "Kundennummer"
    ] = cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"]["Kunde"][
        "Leistungsnummer"
    ] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "Vertragsnummer"
    ] = cpe_maintenance.cpe.carrier_contract_nr.zfill(10)

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"]["Produkt"][
        "ProduktBezeichner"
    ] = cpe_maintenance.cpe.product.subproduct.name

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Vorname"] = cpe_maintenance.contact_provider_fn
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Nachname"] = cpe_maintenance.contact_provider_ln
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Rueckrufnummer"] = cpe_maintenance.contact_provider_tel.as_international
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["EMail"] = cpe_maintenance.contact_provider_email
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Fax"] = cpe_maintenance.contact_provider_fax.as_international

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AngabenZurStoerung"
    ]["BemerkungZurStoerung"]["Bemerkung"] = cpe_maintenance.comment

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"]["Kundentermin"][
        "Datum"
    ] = now.strftime("%Y-%m-%d")

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerOnlineUser"
    ]["Vorname"] = cpe_maintenance.contact_provider_fn
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerOnlineUser"
    ]["Nachname"] = cpe_maintenance.contact_provider_ln
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerOnlineUser"
    ]["Rueckrufnummer"] = cpe_maintenance.contact_provider_tel.as_international

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_complaint(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_complaint.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:annehmenEntstoerauftragRequest"]["Control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["ev:annehmenEntstoerauftragRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "ExterneStoerungsnummer"
    ] = cpe_maintenance.id

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"]["Kunde"][
        "Kundennummer"
    ] = cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"]["Kunde"][
        "Leistungsnummer"
    ] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Vorname"] = cpe_maintenance.contact_provider_fn
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Nachname"] = cpe_maintenance.contact_provider_ln
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Rueckrufnummer"] = cpe_maintenance.contact_provider_tel.as_international
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["EMail"] = cpe_maintenance.contact_provider_email
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerProvider"
    ]["Fax"] = cpe_maintenance.contact_provider_fax.as_international

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "EntstoerauftragRef"
    ] = 1111111111111

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AngabenZurStoerung"
    ]["BemerkungZurStoerung"]["Bemerkung"] = cpe_maintenance.comment

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"]["Kundentermin"][
        "Datum"
    ] = now.strftime("%Y-%m-%d")

    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerOnlineUser"
    ]["Vorname"] = cpe_maintenance.contact_provider_fn
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerOnlineUser"
    ]["Nachname"] = cpe_maintenance.contact_provider_ln
    data["ev:annehmenEntstoerauftragRequest"]["as:Entstoerauftrag"][
        "AnsprechpartnerOnlineUser"
    ]["Rueckrufnummer"] = cpe_maintenance.contact_provider_tel.as_international

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_cancellation(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_cancellation.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:annehmenEntstoerauftragAktionRequest"]["Control"]["zeitstempel"] = (
        now.strftime("%Y-%m-%dT%H:%M:%S.000+02:00")
    )

    data["ev:annehmenEntstoerauftragAktionRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "Kunde"
    ]["Kundennummer"] = cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "Kunde"
    ]["Leistungsnummer"] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "EntstoerauftragReferenz"
    ]["ExterneStoerungsnummer"] = cpe_maintenance.id
    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "EntstoerauftragReferenz"
    ]["Entstoerauftragsnummer"] = "0123456789012"

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_schedule_response(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_schedule_response.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:annehmenEntstoerauftragAktionRequest"]["Control"]["zeitstempel"] = (
        now.strftime("%Y-%m-%dT%H:%M:%S.000+02:00")
    )

    data["ev:annehmenEntstoerauftragAktionRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "Kunde"
    ]["Kundennummer"] = cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "Kunde"
    ]["Leistungsnummer"] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "EntstoerauftragReferenz"
    ]["ExterneStoerungsnummer"] = cpe_maintenance.id
    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "EntstoerauftragReferenz"
    ]["Entstoerauftragsnummer"] = "0123456789012"

    data["ev:annehmenEntstoerauftragAktionRequest"]["ak:EntstoerauftragAktion"][
        "Kundentermin"
    ]["Datum"] = now.strftime("%Y-%m-%d")

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_diagnosis(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_diagnosis.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:annehmenDiagnoseauftragRequest"]["Control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["ev:annehmenDiagnoseauftragRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:annehmenDiagnoseauftragRequest"]["dg:Diagnoseauftrag"]["Kunde"][
        "Kundennummer"
    ] = cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    data["ev:annehmenDiagnoseauftragRequest"]["dg:Diagnoseauftrag"]["Kunde"][
        "Leistungsnummer"
    ] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:annehmenDiagnoseauftragRequest"]["dg:Diagnoseauftrag"][
        "ExterneDiagnosenummer"
    ] = cpe_maintenance.id

    data["ev:annehmenDiagnoseauftragRequest"]["dg:Diagnoseauftrag"][
        "Vertragsnummer"
    ] = cpe_maintenance.cpe.carrier_contract_nr.zfill(10)

    data["ev:annehmenDiagnoseauftragRequest"]["dg:Diagnoseauftrag"][
        "Produktbezeichnung"
    ] = cpe_maintenance.cpe.product.subproduct.name

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_support(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_support.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:annehmenSupportauftragRequest"]["Control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["ev:annehmenSupportauftragRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:annehmenSupportauftragRequest"]["sp:Supportauftrag"]["Kunde"][
        "Kundennummer"
    ] = cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    data["ev:annehmenSupportauftragRequest"]["sp:Supportauftrag"]["Kunde"][
        "Leistungsnummer"
    ] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:annehmenSupportauftragRequest"]["sp:Supportauftrag"][
        "ExterneSupportnummer"
    ] = cpe_maintenance.id

    data["ev:annehmenSupportauftragRequest"]["sp:Supportauftrag"]["Vertragsnummer"] = (
        cpe_maintenance.cpe.carrier_contract_nr.zfill(10)
    )

    data["ev:annehmenSupportauftragRequest"]["sp:Supportauftrag"][
        "Produktbezeichnung"
    ] = cpe_maintenance.cpe.product.subproduct.name

    data["ev:annehmenSupportauftragRequest"]["sp:Supportauftrag"][
        "SupportFunktion"
    ] = "LineReset"

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_guided_diagnosis_start(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_guided_diagnosis_start.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:gefuehrteDiagnoseStartenRequest"]["Control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["ev:gefuehrteDiagnoseStartenRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:gefuehrteDiagnoseStartenRequest"]["Request"]["Kunde"]["Kundennummer"] = (
        cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    )
    data["ev:gefuehrteDiagnoseStartenRequest"]["Request"]["Kunde"][
        "Leistungsnummer"
    ] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:gefuehrteDiagnoseStartenRequest"]["Request"][
        "ExterneNummerGefuehrteDiagnose"
    ] = cpe_maintenance.id

    data["ev:gefuehrteDiagnoseStartenRequest"]["Request"]["Vertragsnummer"] = (
        cpe_maintenance.cpe.carrier_contract_nr.zfill(10)
    )

    data["ev:gefuehrteDiagnoseStartenRequest"]["Request"][
        "Produktbezeichnung"
    ] = cpe_maintenance.cpe.product.subproduct.name

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_guided_diagnosis_continue(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_guided_diagnosis_continue.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:gefuehrteDiagnoseFortsetzenRequest"]["Control"]["zeitstempel"] = (
        now.strftime("%Y-%m-%dT%H:%M:%S.000+02:00")
    )

    data["ev:gefuehrteDiagnoseFortsetzenRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:gefuehrteDiagnoseFortsetzenRequest"]["Request"]["Kunde"][
        "Kundennummer"
    ] = cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    data["ev:gefuehrteDiagnoseFortsetzenRequest"]["Request"]["Kunde"][
        "Leistungsnummer"
    ] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:gefuehrteDiagnoseFortsetzenRequest"]["Request"][
        "ExterneNummerGefuehrteDiagnose"
    ] = cpe_maintenance.id
    data["ev:gefuehrteDiagnoseFortsetzenRequest"]["Request"][
        "ReferenznummerGefuehrteDiagnose"
    ] = 1111111111111

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_maintenance_guided_diagnosis_finish(cpe_maintenance: IspAccessCpeMaintenance):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_maintenance/django_ispstack_api_telekom_de_maintenance/services/v5/templates/maintenance_guided_diagnosis_finish.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ev:gefuehrteDiagnoseBeendenRequest"]["Control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["ev:gefuehrteDiagnoseBeendenRequest"]["Control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    data["ev:gefuehrteDiagnoseBeendenRequest"]["Request"]["Kunde"]["Kundennummer"] = (
        cpe_maintenance.cpe.carrier_customer_nr.zfill(10)
    )
    data["ev:gefuehrteDiagnoseBeendenRequest"]["Request"]["Kunde"][
        "Leistungsnummer"
    ] = cpe_maintenance.cpe.product.carrier_service_nr.zfill(10)

    data["ev:gefuehrteDiagnoseBeendenRequest"]["Request"][
        "ExterneNummerGefuehrteDiagnose"
    ] = cpe_maintenance.id
    data["ev:gefuehrteDiagnoseBeendenRequest"]["Request"][
        "ReferenznummerGefuehrteDiagnose"
    ] = 1111111111111

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def send_maintenance_wholesale(payload: dict, mode: str):
    tk_proxy = (
        settings.TELEKOM_DE_MAINTENANCE_API_PROXY_HOST
        + ":"
        + str(settings.TELEKOM_DE_MAINTENANCE_API_PROXY_PORT)
    )

    proxies = {"http": tk_proxy, "https": tk_proxy}

    rsession = requests.session()
    rsession.proxies.update(proxies)

    # No globally trusted CA
    rsession.verify = False

    transport = Transport(session=rsession)

    client = Client(
        "django_ispstack_api_telekom_de_maintenance\\django_ispstack_api_telekom_de_maintenance\\schema\\05.30.00\\wholesale.wsdl",
        wsse=BinarySignature(
            settings.TELEKOM_DE_MAINTENANCE_CERT_FILE_KEY,
            settings.TELEKOM_DE_MAINTENANCE_CERT_FILE_PUB,
        ),
        transport=transport,
    )

    # Workaround to fix static WSDL
    client.service._binding_options["address"] = (
        settings.TELEKOM_DE_MAINTENANCE_WHOLESALE_API_URL
    )

    return True


def send_maintenance_guided_diagnosis(payload: dict, mode: str):
    tk_proxy = (
        settings.TELEKOM_DE_MAINTENANCE_API_PROXY_HOST
        + ":"
        + str(settings.TELEKOM_DE_MAINTENANCE_API_PROXY_PORT)
    )

    proxies = {"http": tk_proxy, "https": tk_proxy}

    rsession = requests.session()
    rsession.proxies.update(proxies)

    # No globally trusted CA
    rsession.verify = False

    transport = Transport(session=rsession)

    client = Client(
        "django_ispstack_api_telekom_de_maintenance\\django_ispstack_api_telekom_de_maintenance\\schema\\05.30.00\\gefuehrteDiagnose.wsdl",
        wsse=BinarySignature(
            settings.TELEKOM_DE_MAINTENANCE_CERT_FILE_KEY,
            settings.TELEKOM_DE_MAINTENANCE_CERT_FILE_PUB,
        ),
        transport=transport,
    )

    # Workaround to fix static WSDL
    client.service._binding_options["address"] = (
        settings.TELEKOM_DE_MAINTENANCE_DIAG_API_URL
    )

    return True


def send_maintenance_ont_services(payload: dict, mode: str):
    tk_proxy = (
        settings.TELEKOM_DE_MAINTENANCE_API_PROXY_HOST
        + ":"
        + str(settings.TELEKOM_DE_MAINTENANCE_API_PROXY_PORT)
    )

    proxies = {"http": tk_proxy, "https": tk_proxy}

    rsession = requests.session()
    rsession.proxies.update(proxies)

    # No globally trusted CA
    rsession.verify = False

    transport = Transport(session=rsession)

    client = Client(
        "django_ispstack_api_telekom_de_maintenance\\django_ispstack_api_telekom_de_maintenance\\schema\\05.30.00\\ontServices.wsdl",
        wsse=BinarySignature(
            settings.TELEKOM_DE_MAINTENANCE_CERT_FILE_KEY,
            settings.TELEKOM_DE_MAINTENANCE_CERT_FILE_PUB,
        ),
        transport=transport,
    )

    # Workaround to fix static WSDL
    client.service._binding_options["address"] = (
        settings.TELEKOM_DE_MAINTENANCE_ONT_API_URL
    )

    return True
